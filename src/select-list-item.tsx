import createElement from "./util/create-element";
import Component from "./component";
import SelectList from "./select-list";

export interface ItemLi extends HTMLLIElement {
	_SLItem: SelectListItem
}

export default class SelectListItem extends Component {
	static _defaultTagName = 'SelectListItem';
	static observedAttributes = ['title', 'explain', 'hidden'];
	_li: ItemLi;
	_title: HTMLHeadingElement;
	_explain: HTMLParagraphElement;
	constructor() {
		super();
		const li = <li part="item" onclick={ _ => this.click()}><h2 part="title"></h2></li> as ItemLi;
		const title = li.querySelector('h2') as HTMLHeadingElement;
		const explain = <p part="explain"></p> as HTMLParagraphElement;

		this._title = title;
		this._explain = explain;
		this._li = li;

		li._SLItem = this;
	}
	click() {
		const parent = this.parentNode;
		if (!(parent instanceof SelectList)) { return; }
		parent.show(this);
		parent._enter();
	}
	focus() {
		const parent = this.parentNode;
		if (!(parent instanceof SelectList)) { return false; }
		return parent.show(this);
	}
	get explain() { return this.getAttribute('explain') || ''; }
	set explain(explain) { this.setAttribute('explain', explain || ''); }
	attributeChangedCallback(attrName: string, oldVal: string | null, newVal: string | null) {
		if (oldVal === newVal) { return; }
		switch(attrName) {
			case 'title': {
				this._title.innerText = newVal || '';
				break;
			}
			case 'explain': {
				this._explain.innerText = newVal || '';
				if (newVal) {
					this._li.appendChild(this._explain);
				} else {
					this._explain.remove();
				}
				break;
			}
			case 'hidden': {
				if (newVal === null) {
					this._li.removeAttribute('hidden');
				} else {
					this._li.setAttribute('hidden', newVal);
				}
				break;
			}
		}
		const parent = this.parentNode as SelectList;
		if (!(parent instanceof SelectList)) { return; }
		const li = this._li;
		parent._updateShow(li);
		parent._updateSelect(li);
	}
}
