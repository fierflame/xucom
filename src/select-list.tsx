import createElement from "./util/create-element";
import Component from "./component";
import Item, { ItemLi } from "./select-list-item";

export default class SelectList extends Component {
	static Item =Item;
	static _defaultTagName = 'SelectList';
	static observedAttributes = ['placeholder'];
	static _template = <template>
		<style jsx>{`
			:host { display: block; border: 1px solid; }
			input { box-sizing: border-box; border: none; border-bottom: 1px solid; height: 30px; margin: 0; padding: 0 5px; outline: none; width: 100%; }
			ul { position: relative; max-height: 320px; overflow: auto; margin: 0; padding: 0; }
			li { display: none; cursor: pointer; }
			.show { display: block; }
			li * { height: 20px; line-height: 16px; margin: 0; padding: 0; overflow: hidden; white-space:nowrap; text-overflow: ellipsis; }
			h2 { font-size: 12px; padding-top: 4px;}
			p { font-size: 10px; padding-bottom: 4px; }

			li{
				background: var(--xucom-select-list--item--background, #FFF);
				color: var(--xucom-select-list--item--color, inherit);
			}
			li h2{ color: var(--xucom-select-list--item-title--color, inherit); }
			li p{ color: var(--xucom-select-list--item-explain--color, inherit); }
			li:hover{ background: var(--xucom-select-list--item-hover--background, #EEE); }
			li:hover h2{ color: var(--xucom-select-list--item-title-hover--color, inherit); }
			li:hover p{ color: var(--xucom-select-list--item-explain-hover--color, inherit); }
			li.selected{ background: var(--xucom-select-list--item-selected--background, #DDD); }
			li.selected h2{ color: var(--xucom-select-list--item-title-selected--color, inherit); }
			li.selected p{ color: var(--xucom-select-list--item-explain-selected--color, inherit); }
			input{
				background: var(--xucom-select-list--input--background, #FFF);
				color: var(--xucom-select-list--input--color, inherit);
				border-color: var(--xucom-select-list--input--border-color, #BBB);
			}
			input:focus{
				background: var(--xucom-select-list--input-focus--background, #FFF);
				color: var(--xucom-select-list--input-focus--color, inherit);
				border-color: var(--xucom-select-list--input-focus--border-color, #BBB);
			}
		`}</style>
		<input type="search" placeholder="输入内容进行过滤" />
		<ul></ul>
	</template> as HTMLTemplateElement;
	_input: HTMLInputElement;
	_re: RegExp;
	_mutationObserver?: MutationObserver;
	constructor() {
		super();
		let shadow = this.shadowRoot;
		const input = shadow.querySelector('input') as HTMLInputElement;
		this._input = input;
		input.addEventListener('keydown', event => {
			if (event.key === 'ArrowDown') {
				this.next();
			} else if (event.key === 'ArrowUp') {
				this.next(true);
			} else if (event.key === 'Enter') {
				this._enter();
			} else if (event.key === 'Escape') {
				this.dispatchEvent(new Event('cancel'));
			} else {
				return;
			}
			event.preventDefault();
			event.stopPropagation();
		});
		this._re = /.?/i;
		input.addEventListener('input', _ => this.input());
		input.addEventListener('input', _ => this.dispatchEvent(new Event('input')));
	}
	_mutationObserverCallback(list: MutationRecord[]) {
		const ul = this.shadowRoot.querySelector('ul') as HTMLUListElement;
		for (let {addedNodes, nextSibling, removedNodes} of list) {
			let nextli;
			if (nextSibling) { nextli = (nextSibling as Item)._li; }
			for (let item of addedNodes) {
				const li = (item as Item)._li;
				if (nextli) {
					ul.insertBefore(li, nextli);
				} else {
					ul.appendChild(li);
				}
				this._updateShow(li);
				if (this.shadowRoot.querySelectorAll('li.selected').length > 2) {
					li.classList.remove('selected');
					/**
					 * TODO: part
						li.part.remove('selected-item');
						li.firstChild.part.remove('selected-item-title');
						li.lastChild.part.remove('selected-item-explain');
						li.part.remove('selected');
						li.firstChild.part.remove('selected');
						li.lastChild.part.remove('selected');
					 */
				}
			}
			for (let item of removedNodes) {
				(item as Item)._li.remove();
			}
		}
		this._updateSelect();
		this.dispatchEvent(new Event('update'));
	}
	get placeholder() { return this.getAttribute('placeholder'); }
	set placeholder(placeholder) { this.setAttribute('placeholder', placeholder || ''); }
	get value() { return this._input.value; }
	set value(newValue) {
		const input = this._input;
		newValue = String(newValue);
		if (newValue === input.value) { return; }
		input.value = newValue;
		this.input();
	}
	_enter() {
		const li = this.shadowRoot.querySelector('li.selected') as ItemLi | null;
		if (!li) { return false; }
		const item = li._SLItem;
		if (!item) { return false; }
		item.dispatchEvent(new Event('select'));
		item.dispatchEvent(new Event('click'));
		this.dispatchEvent(new Event('change'));
		return true;
	}
	_blur(li?: ItemLi | null) {
		li = li || this.shadowRoot.querySelector('li.selected') as ItemLi | null;
		if (!li) { return false; }
		if(!li.classList.contains('selected')) { return false; }
		li.classList.remove('selected');
		/**
		 * TODO: part
			li.part.remove('selected-item');
		 */
		const item = li._SLItem;
		if (!item) { return false; }
		item.dispatchEvent(new Event('blur'));
		return true;
	}
	_focus(li?: ItemLi) {
		if (!li) { return false; }
		if (li.classList.contains('selected')) { return this.show(li); }
		const item = li._SLItem;
		if (!item) { return false; }
		this._blur();
		li.classList.add('selected');
		/**
		 * TODO: part
			li.part.add('selected-item');
		 */
		this.show(li);
		item.dispatchEvent(new Event('focus'));
		return true;
	}
	focus() { this._input.focus(); }
	show(li) {
		if (li instanceof Item) { return this._focus(li._li); }
		if (!li) { return false; }
		const ul = this.shadowRoot.querySelector('ul') as HTMLUListElement;
		if (ul.scrollTop + ul.clientHeight < li.offsetTop + li.clientHeight) {
			ul.scrollTop = li.offsetTop + li.clientHeight - ul.clientHeight;
		}
		if (ul.scrollTop > li.offsetTop) {
			ul.scrollTop = li.offsetTop;
		}
		return true;
	}
	_updateShow(li) {
		if (!li.hidden && this._re.test(li.innerText.replace(/\s+/g, ' '))) {
			li.classList.add('show');
		} else {
			li.classList.remove('show');
		}
	}
	_updateSelect(li?: HTMLLIElement) {
		if (li) {
			if (li.classList.contains('show')) { return; }
			if (!li.classList.contains('selected')) { return; }
			let item = this.shadowRoot.querySelector('li.selected ~ li.show') || Array.from(this.shadowRoot.querySelectorAll('li.show')).pop();;
			li.classList.remove('selected');
			/**
			 * TODO: part
				li.part.remove('selected-item');
			 */
			if (item) { item.classList.add('selected'); }
			return;
		}
		let selected = this.shadowRoot.querySelector('li.selected') as ItemLi | null;
		if (selected && selected.classList.contains('show')) { return this.show(selected); }
		let item: ItemLi | null | undefined = selected ? this.shadowRoot.querySelector('li.selected ~ li.show') as ItemLi | null : this.shadowRoot.querySelector('li.show') as ItemLi | null;
		this._blur(selected);
		if (!item) { item = Array.from(this.shadowRoot.querySelectorAll('li.show')).pop() as ItemLi | undefined; }
		this._focus(item);
	}
	input() {
		const re = new RegExp (this._input.value.replace(/^\s+/g, '').replace(/\s+$/g, '').replace(/\s+/g, ' ').split('').map(x => x.replace(/([\^\$\(\)\{\}\[\]\.\\\/\?\*\+\|])/,'\\$1')).join('.*'), 'i');
		this._re = re;
		for (let li of Array.from(this.shadowRoot.querySelectorAll('li'))) {
			this._updateShow(li);
		}
		this._updateSelect();
	}
	next(isUp = false) {
		if (!this.shadowRoot.querySelector('li.show')) { return; }
		const list = Array.from(this.shadowRoot.querySelectorAll('li.show, li.selected') as NodeListOf<ItemLi>);
		if (isUp) { list.reverse(); }
		let set = false;
		let item = list[0];
		for (let li of list) {
			if (set) { item = li; break; }
			if (this._blur(li)) { set = true; }
		}
		this._focus(item);
	}
	connectedCallback(){
		this._mutationObserver = new MutationObserver(list => this._mutationObserverCallback(list))
		this._mutationObserver.observe(this, {childList: true});
		const ul = this.shadowRoot.querySelector('ul') as HTMLUListElement;
		const list = Array.from(this.children).filter(li => li instanceof Item && !li.hidden) as Item[];
		ul.remove();
		list.map(li => li._li).forEach(li => ul.appendChild(li));
		list.map(li => this._updateShow(li._li));
		this.shadowRoot.appendChild(ul);
	}
	disconnectedCallback(){
		const ul = this.shadowRoot.querySelector('ul') as HTMLUListElement;
		ul.remove();
		ul.innerHTML = '';
		this.shadowRoot.appendChild(ul);
		if (!this._mutationObserver) { return; }
		this._mutationObserver.disconnect();
		this._mutationObserver = undefined;
	}
	attributeChangedCallback(attrName: string, oldVal: string | null, newVal: string | null){
		if (oldVal === newVal) { return; }
		switch(attrName) {
			case 'placeholder': {
				this._input.placeholder = newVal || '输入内容进行过滤(按上下箭头键选择，回车键确认)';
				return;
			}
		}
	}
}
