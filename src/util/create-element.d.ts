declare namespace CreateElement {

    interface HTMLAttributes {
        className?: string | string[],
        class?: string,
        id?: string,
        part?: string,
        onabort?: ((this: HTMLElement, ev: UIEvent) => any) | null;
        onanimationcancel?: ((this: HTMLElement, ev: AnimationEvent) => any) | null;
        onanimationend?: ((this: HTMLElement, ev: AnimationEvent) => any) | null;
        onanimationiteration?: ((this: HTMLElement, ev: AnimationEvent) => any) | null;
        onanimationstart?: ((this: HTMLElement, ev: AnimationEvent) => any) | null;
        onauxclick?: ((this: HTMLElement, ev: MouseEvent) => any) | null;
        onblur?: ((this: HTMLElement, ev: FocusEvent) => any) | null;
        oncancel?: ((this: HTMLElement, ev: Event) => any) | null;
        oncanplay?: ((this: HTMLElement, ev: Event) => any) | null;
        oncanplaythrough?: ((this: HTMLElement, ev: Event) => any) | null;
        onchange?: ((this: HTMLElement, ev: Event) => any) | null;
        onclick?: ((this: HTMLElement, ev: MouseEvent) => any) | null;
        onclose?: ((this: HTMLElement, ev: Event) => any) | null;
        oncontextmenu?: ((this: HTMLElement, ev: MouseEvent) => any) | null;
        oncuechange?: ((this: HTMLElement, ev: Event) => any) | null;
        ondblclick?: ((this: HTMLElement, ev: MouseEvent) => any) | null;
        ondrag?: ((this: HTMLElement, ev: DragEvent) => any) | null;
        ondragend?: ((this: HTMLElement, ev: DragEvent) => any) | null;
        ondragenter?: ((this: HTMLElement, ev: DragEvent) => any) | null;
        ondragexit?: ((this: HTMLElement, ev: Event) => any) | null;
        ondragleave?: ((this: HTMLElement, ev: DragEvent) => any) | null;
        ondragover?: ((this: HTMLElement, ev: DragEvent) => any) | null;
        ondragstart?: ((this: HTMLElement, ev: DragEvent) => any) | null;
        ondrop?: ((this: HTMLElement, ev: DragEvent) => any) | null;
        ondurationchange?: ((this: HTMLElement, ev: Event) => any) | null;
        onemptied?: ((this: HTMLElement, ev: Event) => any) | null;
        onended?: ((this: HTMLElement, ev: Event) => any) | null;
        onerror?: OnErrorEventHandler;
        onfocus?: ((this: HTMLElement, ev: FocusEvent) => any) | null;
        ongotpointercapture?: ((this: HTMLElement, ev: PointerEvent) => any) | null;
        oninput?: ((this: HTMLElement, ev: Event) => any) | null;
        oninvalid?: ((this: HTMLElement, ev: Event) => any) | null;
        onkeydown?: ((this: HTMLElement, ev: KeyboardEvent) => any) | null;
        onkeypress?: ((this: HTMLElement, ev: KeyboardEvent) => any) | null;
        onkeyup?: ((this: HTMLElement, ev: KeyboardEvent) => any) | null;
        onload?: ((this: HTMLElement, ev: Event) => any) | null;
        onloadeddata?: ((this: HTMLElement, ev: Event) => any) | null;
        onloadedmetadata?: ((this: HTMLElement, ev: Event) => any) | null;
        onloadend?: ((this: HTMLElement, ev: ProgressEvent) => any) | null;
        onloadstart?: ((this: HTMLElement, ev: Event) => any) | null;
        onlostpointercapture?: ((this: HTMLElement, ev: PointerEvent) => any) | null;
        onmousedown?: ((this: HTMLElement, ev: MouseEvent) => any) | null;
        onmouseenter?: ((this: HTMLElement, ev: MouseEvent) => any) | null;
        onmouseleave?: ((this: HTMLElement, ev: MouseEvent) => any) | null;
        onmousemove?: ((this: HTMLElement, ev: MouseEvent) => any) | null;
        onmouseout?: ((this: HTMLElement, ev: MouseEvent) => any) | null;
        onmouseover?: ((this: HTMLElement, ev: MouseEvent) => any) | null;
        onmouseup?: ((this: HTMLElement, ev: MouseEvent) => any) | null;
        onpause?: ((this: HTMLElement, ev: Event) => any) | null;
        onplay?: ((this: HTMLElement, ev: Event) => any) | null;
        onplaying?: ((this: HTMLElement, ev: Event) => any) | null;
        onpointercancel?: ((this: HTMLElement, ev: PointerEvent) => any) | null;
        onpointerdown?: ((this: HTMLElement, ev: PointerEvent) => any) | null;
        onpointerenter?: ((this: HTMLElement, ev: PointerEvent) => any) | null;
        onpointerleave?: ((this: HTMLElement, ev: PointerEvent) => any) | null;
        onpointermove?: ((this: HTMLElement, ev: PointerEvent) => any) | null;
        onpointerout?: ((this: HTMLElement, ev: PointerEvent) => any) | null;
        onpointerover?: ((this: HTMLElement, ev: PointerEvent) => any) | null;
        onpointerup?: ((this: HTMLElement, ev: PointerEvent) => any) | null;
        onprogress?: ((this: HTMLElement, ev: ProgressEvent) => any) | null;
        onratechange?: ((this: HTMLElement, ev: Event) => any) | null;
        onreset?: ((this: HTMLElement, ev: Event) => any) | null;
        onresize?: ((this: HTMLElement, ev: UIEvent) => any) | null;
        onscroll?: ((this: HTMLElement, ev: Event) => any) | null;
        onsecuritypolicyviolation?: ((this: HTMLElement, ev: SecurityPolicyViolationEvent) => any) | null;
        onseeked?: ((this: HTMLElement, ev: Event) => any) | null;
        onseeking?: ((this: HTMLElement, ev: Event) => any) | null;
        onselect?: ((this: HTMLElement, ev: Event) => any) | null;
        onselectionchange?: ((this: HTMLElement, ev: Event) => any) | null;
        onselectstart?: ((this: HTMLElement, ev: Event) => any) | null;
        onstalled?: ((this: HTMLElement, ev: Event) => any) | null;
        onsubmit?: ((this: HTMLElement, ev: Event) => any) | null;
        onsuspend?: ((this: HTMLElement, ev: Event) => any) | null;
        ontimeupdate?: ((this: HTMLElement, ev: Event) => any) | null;
        ontoggle?: ((this: HTMLElement, ev: Event) => any) | null;
        ontouchcancel?: ((this: HTMLElement, ev: TouchEvent) => any) | null;
        ontouchend?: ((this: HTMLElement, ev: TouchEvent) => any) | null;
        ontouchmove?: ((this: HTMLElement, ev: TouchEvent) => any) | null;
        ontouchstart?: ((this: HTMLElement, ev: TouchEvent) => any) | null;
        ontransitioncancel?: ((this: HTMLElement, ev: TransitionEvent) => any) | null;
        ontransitionend?: ((this: HTMLElement, ev: TransitionEvent) => any) | null;
        ontransitionrun?: ((this: HTMLElement, ev: TransitionEvent) => any) | null;
        ontransitionstart?: ((this: HTMLElement, ev: TransitionEvent) => any) | null;
        onvolumechange?: ((this: HTMLElement, ev: Event) => any) | null;
        onwaiting?: ((this: HTMLElement, ev: Event) => any) | null;
        onwheel?: ((this: HTMLElement, ev: WheelEvent) => any) | null;
        // CreateElement-specific Attributes
        defaultChecked?: boolean;
        defaultValue?: string | string[];
        suppressContentEditableWarning?: boolean;
        suppressHydrationWarning?: boolean;

        // Standard HTML Attributes
        accessKey?: string;
        contentEditable?: boolean;
        contextMenu?: string;
        dir?: string;
        draggable?: boolean;
        hidden?: boolean;
        lang?: string;
        placeholder?: string;
        slot?: string;
        spellCheck?: boolean;
        style?: string;
        tabIndex?: number;
        title?: string;

        // Unknown
        inputMode?: string;
        is?: string;
        radioGroup?: string; // <command>, <menuitem>

        // WAI-ARIA
        role?: string;

        // RDFa Attributes
        about?: string;
        datatype?: string;
        inlist?: any;
        prefix?: string;
        property?: string;
        resource?: string;
        typeof?: string;
        vocab?: string;

        // Non-standard Attributes
        autoCapitalize?: string;
        autoCorrect?: string;
        autoSave?: string;
        color?: string;
        itemProp?: string;
        itemScope?: boolean;
        itemType?: string;
        itemID?: string;
        itemRef?: string;
        results?: number;
        security?: string;
        unselectable?: 'on' | 'off';
        'aria-activedescendant'?: string;
        'aria-atomic'?: boolean | 'false' | 'true';
        'aria-autocomplete'?: 'none' | 'inline' | 'list' | 'both';
        'aria-busy'?: boolean | 'false' | 'true';
        'aria-checked'?: boolean | 'false' | 'mixed' | 'true';
        'aria-colcount'?: number;
        'aria-colindex'?: number;
        'aria-colspan'?: number;
        'aria-controls'?: string;
        'aria-current'?: boolean | 'false' | 'true' | 'page' | 'step' | 'location' | 'date' | 'time';
        'aria-describedby'?: string;
        'aria-details'?: string;
        'aria-disabled'?: boolean | 'false' | 'true';
        'aria-dropeffect'?: 'none' | 'copy' | 'execute' | 'link' | 'move' | 'popup';
        'aria-errormessage'?: string;
        'aria-expanded'?: boolean | 'false' | 'true';
        'aria-flowto'?: string;
        'aria-grabbed'?: boolean | 'false' | 'true';
        'aria-haspopup'?: boolean | 'false' | 'true' | 'menu' | 'listbox' | 'tree' | 'grid' | 'dialog';
        'aria-hidden'?: boolean | 'false' | 'true';
        'aria-invalid'?: boolean | 'false' | 'true' | 'grammar' | 'spelling';
        'aria-keyshortcuts'?: string;
        'aria-label'?: string;
        'aria-labelledby'?: string;
        'aria-level'?: number;
        'aria-live'?: 'off' | 'assertive' | 'polite';
        'aria-modal'?: boolean | 'false' | 'true';
        'aria-multiline'?: boolean | 'false' | 'true';
        'aria-multiselectable'?: boolean | 'false' | 'true';
        'aria-orientation'?: 'horizontal' | 'vertical';
        'aria-owns'?: string;
        'aria-placeholder'?: string;
        'aria-posinset'?: number;
        'aria-pressed'?: boolean | 'false' | 'mixed' | 'true';
        'aria-readonly'?: boolean | 'false' | 'true';
        'aria-relevant'?: 'additions' | 'additions text' | 'all' | 'removals' | 'text';
        'aria-required'?: boolean | 'false' | 'true';
        'aria-roledescription'?: string;
        'aria-rowcount'?: number;
        'aria-rowindex'?: number;
        'aria-rowspan'?: number;
        'aria-selected'?: boolean | 'false' | 'true';
        'aria-setsize'?: number;
        'aria-sort'?: 'none' | 'ascending' | 'descending' | 'other';
        'aria-valuemax'?: number;
        'aria-valuemin'?: number;
        'aria-valuenow'?: number;
        'aria-valuetext'?: string;
    }

    interface AllHTMLAttributes extends HTMLAttributes {
        // Standard HTML Attributes
        accept?: string;
        acceptCharset?: string;
        action?: string;
        allowFullScreen?: boolean;
        allowTransparency?: boolean;
        alt?: string;
        as?: string;
        async?: boolean;
        autoComplete?: string;
        autoFocus?: boolean;
        autoPlay?: boolean;
        capture?: boolean | string;
        cellPadding?: number | string;
        cellSpacing?: number | string;
        charSet?: string;
        challenge?: string;
        checked?: boolean;
        cite?: string;
        classID?: string;
        cols?: number;
        colSpan?: number;
        content?: string;
        controls?: boolean;
        coords?: string;
        crossOrigin?: string;
        data?: string;
        dateTime?: string;
        default?: boolean;
        defer?: boolean;
        disabled?: boolean;
        download?: any;
        encType?: string;
        form?: string;
        formAction?: string;
        formEncType?: string;
        formMethod?: string;
        formNoValidate?: boolean;
        formTarget?: string;
        frameBorder?: number | string;
        headers?: string;
        height?: number | string;
        high?: number;
        href?: string;
        hrefLang?: string;
        htmlFor?: string;
        httpEquiv?: string;
        integrity?: string;
        keyParams?: string;
        keyType?: string;
        kind?: string;
        label?: string;
        list?: string;
        loop?: boolean;
        low?: number;
        manifest?: string;
        marginHeight?: number;
        marginWidth?: number;
        max?: number | string;
        maxLength?: number;
        media?: string;
        mediaGroup?: string;
        method?: string;
        min?: number | string;
        minLength?: number;
        multiple?: boolean;
        muted?: boolean;
        name?: string;
        nonce?: string;
        noValidate?: boolean;
        open?: boolean;
        optimum?: number;
        pattern?: string;
        placeholder?: string;
        playsInline?: boolean;
        poster?: string;
        preload?: string;
        readOnly?: boolean;
        rel?: string;
        required?: boolean;
        reversed?: boolean;
        rows?: number;
        rowSpan?: number;
        sandbox?: string;
        scope?: string;
        scoped?: boolean;
        scrolling?: string;
        seamless?: boolean;
        selected?: boolean;
        shape?: string;
        size?: number;
        sizes?: string;
        span?: number;
        src?: string;
        srcDoc?: string;
        srcLang?: string;
        srcSet?: string;
        start?: number;
        step?: number | string;
        summary?: string;
        target?: string;
        type?: string;
        useMap?: string;
        value?: string | string[] | number;
        width?: number | string;
        wmode?: string;
        wrap?: string;
    }

    interface AnchorHTMLAttributes extends HTMLAttributes {
        download?: any;
        href?: string;
        hrefLang?: string;
        media?: string;
        rel?: string;
        target?: string;
        type?: string;
        referrerPolicy?: string;
    }

    // tslint:disable-next-line:no-empty-interface
    interface AudioHTMLAttributes extends MediaHTMLAttributes {}

    interface AreaHTMLAttributes extends HTMLAttributes {
        alt?: string;
        coords?: string;
        download?: any;
        href?: string;
        hrefLang?: string;
        media?: string;
        rel?: string;
        shape?: string;
        target?: string;
    }

    interface BaseHTMLAttributes extends HTMLAttributes {
        href?: string;
        target?: string;
    }

    interface BlockquoteHTMLAttributes extends HTMLAttributes {
        cite?: string;
    }

    interface ButtonHTMLAttributes extends HTMLAttributes {
        autoFocus?: boolean;
        disabled?: boolean;
        form?: string;
        formAction?: string;
        formEncType?: string;
        formMethod?: string;
        formNoValidate?: boolean;
        formTarget?: string;
        name?: string;
        type?: 'submit' | 'reset' | 'button';
        value?: string | string[] | number;
    }

    interface CanvasHTMLAttributes extends HTMLAttributes {
        height?: number | string;
        width?: number | string;
    }

    interface ColHTMLAttributes extends HTMLAttributes {
        span?: number;
        width?: number | string;
    }

    interface ColgroupHTMLAttributes extends HTMLAttributes {
        span?: number;
    }

    interface DetailsHTMLAttributes extends HTMLAttributes {
        open?: boolean;
    }

    interface DelHTMLAttributes extends HTMLAttributes {
        cite?: string;
        dateTime?: string;
    }

    interface DialogHTMLAttributes extends HTMLAttributes {
        open?: boolean;
    }

    interface EmbedHTMLAttributes extends HTMLAttributes {
        height?: number | string;
        src?: string;
        type?: string;
        width?: number | string;
    }

    interface FieldsetHTMLAttributes extends HTMLAttributes {
        disabled?: boolean;
        form?: string;
        name?: string;
    }

    interface FormHTMLAttributes extends HTMLAttributes {
        acceptCharset?: string;
        action?: string;
        autoComplete?: string;
        encType?: string;
        method?: string;
        name?: string;
        noValidate?: boolean;
        target?: string;
    }

    interface HtmlHTMLAttributes extends HTMLAttributes {
        manifest?: string;
    }

    interface IframeHTMLAttributes extends HTMLAttributes {
        allow?: string;
        allowFullScreen?: boolean;
        allowTransparency?: boolean;
        frameBorder?: number | string;
        height?: number | string;
        marginHeight?: number;
        marginWidth?: number;
        name?: string;
        sandbox?: string;
        scrolling?: string;
        seamless?: boolean;
        src?: string;
        srcDoc?: string;
        width?: number | string;
    }

    interface ImgHTMLAttributes extends HTMLAttributes {
        alt?: string;
        crossOrigin?: "anonymous" | "use-credentials" | "";
        decoding?: "async" | "auto" | "sync";
        height?: number | string;
        sizes?: string;
        src?: string;
        srcSet?: string;
        useMap?: string;
        width?: number | string;
    }

    interface InsHTMLAttributes extends HTMLAttributes {
        cite?: string;
        dateTime?: string;
    }

    interface InputHTMLAttributes extends HTMLAttributes {
        accept?: string;
        alt?: string;
        autoComplete?: string;
        autoFocus?: boolean;
        capture?: boolean | string; // https://www.w3.org/TR/html-media-capture/#the-capture-attribute
        checked?: boolean;
        crossOrigin?: string;
        disabled?: boolean;
        form?: string;
        formAction?: string;
        formEncType?: string;
        formMethod?: string;
        formNoValidate?: boolean;
        formTarget?: string;
        height?: number | string;
        list?: string;
        max?: number | string;
        maxLength?: number;
        min?: number | string;
        minLength?: number;
        multiple?: boolean;
        name?: string;
        pattern?: string;
        placeholder?: string;
        readOnly?: boolean;
        required?: boolean;
        size?: number;
        src?: string;
        step?: number | string;
        type?: string;
        value?: string | string[] | number;
        width?: number | string;

        onChange?: Event;
    }

    interface KeygenHTMLAttributes extends HTMLAttributes {
        autoFocus?: boolean;
        challenge?: string;
        disabled?: boolean;
        form?: string;
        keyType?: string;
        keyParams?: string;
        name?: string;
    }

    interface LabelHTMLAttributes extends HTMLAttributes {
        form?: string;
        htmlFor?: string;
    }

    interface LiHTMLAttributes extends HTMLAttributes {
        value?: string | string[] | number;
    }

    interface LinkHTMLAttributes extends HTMLAttributes {
        as?: string;
        crossOrigin?: string;
        href?: string;
        hrefLang?: string;
        integrity?: string;
        media?: string;
        rel?: string;
        sizes?: string;
        type?: string;
    }

    interface MapHTMLAttributes extends HTMLAttributes {
        name?: string;
    }

    interface MenuHTMLAttributes extends HTMLAttributes {
        type?: string;
    }

    interface MediaHTMLAttributes extends HTMLAttributes {
        autoPlay?: boolean;
        controls?: boolean;
        controlsList?: string;
        crossOrigin?: string;
        loop?: boolean;
        mediaGroup?: string;
        muted?: boolean;
        playsinline?: boolean;
        preload?: string;
        src?: string;
    }

    interface MetaHTMLAttributes extends HTMLAttributes {
        charSet?: string;
        content?: string;
        httpEquiv?: string;
        name?: string;
    }

    interface MeterHTMLAttributes extends HTMLAttributes {
        form?: string;
        high?: number;
        low?: number;
        max?: number | string;
        min?: number | string;
        optimum?: number;
        value?: string | string[] | number;
    }

    interface QuoteHTMLAttributes extends HTMLAttributes {
        cite?: string;
    }

    interface ObjectHTMLAttributes extends HTMLAttributes {
        classID?: string;
        data?: string;
        form?: string;
        height?: number | string;
        name?: string;
        type?: string;
        useMap?: string;
        width?: number | string;
        wmode?: string;
    }

    interface OlHTMLAttributes extends HTMLAttributes {
        reversed?: boolean;
        start?: number;
        type?: '1' | 'a' | 'A' | 'i' | 'I';
    }

    interface OptgroupHTMLAttributes extends HTMLAttributes {
        disabled?: boolean;
        label?: string;
    }

    interface OptionHTMLAttributes extends HTMLAttributes {
        disabled?: boolean;
        label?: string;
        selected?: boolean;
        value?: string | string[] | number;
    }

    interface OutputHTMLAttributes extends HTMLAttributes {
        form?: string;
        htmlFor?: string;
        name?: string;
    }

    interface ParamHTMLAttributes extends HTMLAttributes {
        name?: string;
        value?: string | string[] | number;
    }

    interface ProgressHTMLAttributes extends HTMLAttributes {
        max?: number | string;
        value?: string | string[] | number;
    }

    interface ScriptHTMLAttributes extends HTMLAttributes {
        async?: boolean;
        charSet?: string;
        crossOrigin?: string;
        defer?: boolean;
        integrity?: string;
        noModule?: boolean;
        nonce?: string;
        src?: string;
        type?: string;
    }

    interface SelectHTMLAttributes extends HTMLAttributes {
        autoComplete?: string;
        autoFocus?: boolean;
        disabled?: boolean;
        form?: string;
        multiple?: boolean;
        name?: string;
        required?: boolean;
        size?: number;
        value?: string | string[] | number;
        onChange?: Event;
    }

    interface SourceHTMLAttributes extends HTMLAttributes {
        media?: string;
        sizes?: string;
        src?: string;
        srcSet?: string;
        type?: string;
    }

    interface StyleHTMLAttributes extends HTMLAttributes {
        media?: string;
        nonce?: string;
        scoped?: boolean;
        type?: string;
        jsx?: boolean;
    }

    interface TableHTMLAttributes extends HTMLAttributes {
        cellPadding?: number | string;
        cellSpacing?: number | string;
        summary?: string;
    }

    interface TextareaHTMLAttributes extends HTMLAttributes {
        autoComplete?: string;
        autoFocus?: boolean;
        cols?: number;
        dirName?: string;
        disabled?: boolean;
        form?: string;
        maxLength?: number;
        minLength?: number;
        name?: string;
        placeholder?: string;
        readOnly?: boolean;
        required?: boolean;
        rows?: number;
        value?: string | string[] | number;
        wrap?: string;

        onChange?: Event;
    }

    interface TdHTMLAttributes extends HTMLAttributes {
        align?: "left" | "center" | "right" | "justify" | "char";
        colSpan?: number;
        headers?: string;
        rowSpan?: number;
        scope?: string;
    }

    interface ThHTMLAttributes extends HTMLAttributes {
        align?: "left" | "center" | "right" | "justify" | "char";
        colSpan?: number;
        headers?: string;
        rowSpan?: number;
        scope?: string;
    }

    interface TimeHTMLAttributes extends HTMLAttributes {
        dateTime?: string;
    }

    interface TrackHTMLAttributes extends HTMLAttributes {
        default?: boolean;
        kind?: string;
        label?: string;
        src?: string;
        srcLang?: string;
    }

    interface VideoHTMLAttributes extends MediaHTMLAttributes {
        height?: number | string;
        playsInline?: boolean;
        poster?: string;
        width?: number | string;
    }
    interface JSXElement extends Element {}
}

declare namespace JSX {
    type Element = CreateElement.JSXElement;
    interface Elements {
        div: HTMLDivElement;
    }
    type DivElement = HTMLDivElement;
    interface IntrinsicElements {
        // HTML
        a:CreateElement.AnchorHTMLAttributes;
        abbr:CreateElement.HTMLAttributes;
        address:CreateElement.HTMLAttributes;
        area:CreateElement.AreaHTMLAttributes;
        article:CreateElement.HTMLAttributes;
        aside:CreateElement.HTMLAttributes;
        audio:CreateElement.AudioHTMLAttributes;
        b:CreateElement.HTMLAttributes;
        base:CreateElement.BaseHTMLAttributes;
        bdi:CreateElement.HTMLAttributes;
        bdo:CreateElement.HTMLAttributes;
        big:CreateElement.HTMLAttributes;
        blockquote:CreateElement.BlockquoteHTMLAttributes;
        body:CreateElement.HTMLAttributes;
        br:CreateElement.HTMLAttributes;
        button:CreateElement.ButtonHTMLAttributes;
        canvas:CreateElement.CanvasHTMLAttributes;
        caption:CreateElement.HTMLAttributes;
        cite:CreateElement.HTMLAttributes;
        code:CreateElement.HTMLAttributes;
        col:CreateElement.ColHTMLAttributes;
        colgroup:CreateElement.ColgroupHTMLAttributes;
        data:CreateElement.HTMLAttributes;
        datalist:CreateElement.HTMLAttributes;
        dd:CreateElement.HTMLAttributes;
        del:CreateElement.DelHTMLAttributes;
        details:CreateElement.DetailsHTMLAttributes;
        dfn:CreateElement.HTMLAttributes;
        dialog:CreateElement.DialogHTMLAttributes;
        div:CreateElement.HTMLAttributes;
        dl:CreateElement.HTMLAttributes;
        dt:CreateElement.HTMLAttributes;
        em:CreateElement.HTMLAttributes;
        embed:CreateElement.EmbedHTMLAttributes;
        fieldset:CreateElement.FieldsetHTMLAttributes;
        figcaption:CreateElement.HTMLAttributes;
        figure:CreateElement.HTMLAttributes;
        footer:CreateElement.HTMLAttributes;
        form:CreateElement.FormHTMLAttributes;
        h1:CreateElement.HTMLAttributes;
        h2:CreateElement.HTMLAttributes;
        h3:CreateElement.HTMLAttributes;
        h4:CreateElement.HTMLAttributes;
        h5:CreateElement.HTMLAttributes;
        h6:CreateElement.HTMLAttributes;
        head:CreateElement.HTMLAttributes;
        header:CreateElement.HTMLAttributes;
        hgroup:CreateElement.HTMLAttributes;
        hr:CreateElement.HTMLAttributes;
        html:CreateElement.HtmlHTMLAttributes;
        i:CreateElement.HTMLAttributes;
        iframe:CreateElement.IframeHTMLAttributes;
        img:CreateElement.ImgHTMLAttributes;
        input:CreateElement.InputHTMLAttributes;
        ins:CreateElement.InsHTMLAttributes;
        kbd:CreateElement.HTMLAttributes;
        keygen:CreateElement.KeygenHTMLAttributes;
        label:CreateElement.LabelHTMLAttributes;
        legend:CreateElement.HTMLAttributes;
        li:CreateElement.LiHTMLAttributes;
        link:CreateElement.LinkHTMLAttributes;
        main:CreateElement.HTMLAttributes;
        map:CreateElement.MapHTMLAttributes;
        mark:CreateElement.HTMLAttributes;
        menu:CreateElement.MenuHTMLAttributes;
        menuitem:CreateElement.HTMLAttributes;
        meta:CreateElement.MetaHTMLAttributes;
        meter:CreateElement.MeterHTMLAttributes;
        nav:CreateElement.HTMLAttributes;
        noindex:CreateElement.HTMLAttributes;
        noscript:CreateElement.HTMLAttributes;
        object:CreateElement.ObjectHTMLAttributes;
        ol:CreateElement.OlHTMLAttributes;
        optgroup:CreateElement.OptgroupHTMLAttributes;
        option:CreateElement.OptionHTMLAttributes;
        output:CreateElement.OutputHTMLAttributes;
        p:CreateElement.HTMLAttributes;
        param:CreateElement.ParamHTMLAttributes;
        picture:CreateElement.HTMLAttributes;
        pre:CreateElement.HTMLAttributes;
        progress:CreateElement.ProgressHTMLAttributes;
        q:CreateElement.QuoteHTMLAttributes;
        rp:CreateElement.HTMLAttributes;
        rt:CreateElement.HTMLAttributes;
        ruby:CreateElement.HTMLAttributes;
        s:CreateElement.HTMLAttributes;
        samp:CreateElement.HTMLAttributes;
        script:CreateElement.ScriptHTMLAttributes;
        section:CreateElement.HTMLAttributes;
        select:CreateElement.SelectHTMLAttributes;
        small:CreateElement.HTMLAttributes;
        source:CreateElement.SourceHTMLAttributes;
        span:CreateElement.HTMLAttributes;
        strong:CreateElement.HTMLAttributes;
        style:CreateElement.StyleHTMLAttributes;
        sub:CreateElement.HTMLAttributes;
        summary:CreateElement.HTMLAttributes;
        sup:CreateElement.HTMLAttributes;
        table:CreateElement.TableHTMLAttributes;
        tbody:CreateElement.HTMLAttributes;
        td:CreateElement.TdHTMLAttributes;
        textarea:CreateElement.TextareaHTMLAttributes;
        tfoot:CreateElement.HTMLAttributes;
        th:CreateElement.ThHTMLAttributes;
        thead:CreateElement.HTMLAttributes;
        time:CreateElement.TimeHTMLAttributes;
        title:CreateElement.HTMLAttributes;
        tr:CreateElement.HTMLAttributes;
        track:CreateElement.TrackHTMLAttributes;
        u:CreateElement.HTMLAttributes;
        ul:CreateElement.HTMLAttributes;
        "var":CreateElement.HTMLAttributes;
        video:CreateElement.VideoHTMLAttributes;
        wbr:CreateElement.HTMLAttributes;
        [tagName: string]:CreateElement.HTMLAttributes & {[key: string]: any};
    }
}
