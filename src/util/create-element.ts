/// <reference path="create-element.d.ts" />

import Component from "../component";

interface Attrs{
	[key: string]: number | boolean | string | EventListener | string[] | undefined | null;
}

function append(el: Node, children: any[]) {
	for (let child of children) {
		if (child instanceof HTMLElement) {
			el.appendChild(child);
		} else if (Array.isArray(child)) {
			append(el, child);
		} else if (child !== null && child !== undefined) {
			el.appendChild(document.createTextNode(child));
		}
	}
}

function createElement<K extends keyof HTMLElementTagNameMap>(tagName: K, attrs?:Attrs, ...children: any[]): HTMLElementTagNameMap[K];
function createElement(tagName: string | typeof Component, attrs?:Attrs, ...children: any[]): HTMLElement;
function createElement(tagName: string | typeof Component, attrs?:Attrs, ...children: any[]): HTMLElement {
	if(Object.prototype.isPrototypeOf.call(Component, tagName)) {
		let component = tagName as typeof Component;
		tagName = component.registerCustomElement();
		while (!tagName) {
			tagName = component.registerCustomElement(String(Math.floor(Math.random() * 10000000)));
		}
	}
	tagName = (tagName as string).toLowerCase();
	attrs = attrs || {};
	if (tagName === 'style') { delete attrs.jsx; }	
	let is = attrs.is || '';
	const el = is && typeof is === 'string' ? document.createElement(tagName, {is}) : document.createElement(tagName);
	for (let k in attrs) {
		if (k === 'classList') { k = 'class'; }
		const value = attrs[k];
		if (typeof value === 'string' || typeof value === 'number') {
			el.setAttribute(k.replace(/[A-Z]/g, '-$1'), String(value));
		} else if (typeof value === 'boolean') {
			if (value) { el.setAttribute(k.replace(/[A-Z]/g, '-$1'), ''); }
		} else if (Array.isArray(value)) {
			el.setAttribute(k.replace(/[A-Z]/g, '-$1'), value.join(' '));
		} else if (k.substr(0,2) === 'on' && typeof value === 'function') {
			k = k.substr(2);
			el.addEventListener(k, value);
		}
	}
	append(tagName === 'template'? (el as HTMLTemplateElement).content : el, children);
	return el;
}
export default createElement;