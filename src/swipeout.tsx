import createElement from "./util/create-element";
import Component from "./component";
interface DOMRectReadOnly {
	readonly x: number;
	readonly y: number;
	readonly width: number;
	readonly height: number;
	readonly top: number;
	readonly right: number;
	readonly bottom: number;
	readonly left: number;
}

interface ResizeObserver {
    disconnect(): void;
    observe(target: Element | SVGElement): void;
    unobserve(target: Element | SVGElement): void;
}
interface ResizeObserverCallback {
    (ResizeObserverEntry: {contentRect: DOMRectReadOnly, target: Element | SVGElement}): void;
}
declare const ResizeObserver: {
    prototype: ResizeObserver;
    new(callback: ResizeObserverCallback): ResizeObserver;
}
export default class Swipeout extends Component {
	static _defaultTagName = 'Swipeout';
	static observedAttributes = ['unclosable', 'width', 'opened'];
	static _template = <template>
		<style jsx>{`
		:host {
			position: relative;
			display: block;
			overflow: hidden;
		}
		.buttons {
			display: flex;
			position: absolute;
			left: 100%;
			top: 0;
			bottom: 0;
			width: var(--xucom-swipeout--width, auto);
		}
		`}</style>
	<div class="main">
		<slot></slot>
		<div class="buttons" part="buttons"><slot name="button"></slot></div>
	</div>
	</template> as HTMLTemplateElement;
	_main: HTMLDivElement;
	_buttons: HTMLDivElement;
	_ro?: ResizeObserver;
	constructor() {
		super();
		const shadow = this.shadowRoot;
		const main = this._main = shadow.querySelector('.main') as HTMLDivElement;
		const buttons = this._buttons = shadow.querySelector('.buttons') as HTMLDivElement;

		buttons.addEventListener('click', _ => !this.unclosable && this.close());

		main.addEventListener('touchstart', event => this._start(event));
		main.addEventListener('touchmove', event => this._move(event));
		main.addEventListener('touchend', event => this._end(event));


		main.addEventListener('mousedown', event => this._start(event));
		document.addEventListener('mousemove', event => this._move(event));
		document.addEventListener('mouseup', event => this._end(event));
	}
	_run: boolean = false;
	_toClose: boolean = false;
	_toOpen: boolean = false;
	_tStart: number;
	_start({touches, pageX}: {touches?: TouchList, pageX?: number}) {
		if (touches) {
			const touche = touches && touches[0];
			if( !touche ){ return; }
			pageX = touche.pageX;
		} else {
			this._run = true;
		}
		this._tStart = pageX as number;
		this._toClose = false;
		this._toOpen = false;
	}
	_move({touches, pageX}: {touches?: TouchList, pageX?: number}) {
		if (touches) {
			const touche = touches && touches[0];
			if( !touche ){ return; }
			pageX = touche.pageX;
		} else {
			if (!this._run) {return;} 
		}
		let {position, width, opened, _tStart, _toClose, _toOpen} = this;
		let newPosition = _tStart - (pageX as number);
		if (opened) { newPosition += width; }
		_toClose = newPosition < position || newPosition <= 0;
		_toOpen = newPosition > position || newPosition >= width;
		position = Math.min(newPosition, width);
		if (position < 0) {position = 0;}
		this.position = position;
		this._toClose = _toClose;
		this._toOpen = _toOpen;
	}
	_end({touches}: {touches?: TouchList, pageX?: number}) {
		if (touches) {
			const touche = touches && touches[0];
			if( touche ){ return; }
		} else {
			if (!this._run) {return;}
			this._run = false;
		}
		const {width, _toClose, _toOpen, opened, position} = this;
		if (_toClose && position < width / 4 * 3) { return this.close(); }
		if (_toOpen && position >= width / 4) { return this.open(); }
		if (opened) { return this.open(); }
		return this.close();
	}
	_opened: boolean = false;
	close() {
		this.position = 0;
		this._opened = false;
		this.removeAttribute('opened');
	}
	open() {
		this.position = this.width;
		this._opened = true;
		this.setAttribute('opened', '');
	}
	_position: number = 0;
	get position() {
		return this._position;
	}
	set position(v) {
		this._position = v;
		this._main.style.transform = `translate(-${v}px,0)`;
	}
	get opened() {
		return this._opened;
	}
	get width(): number {
		return this._buttons.offsetWidth;
	}
	set width(w: number) {
		if (w) {
			this.setAttribute('width', String(w));
		} else {
			this.removeAttribute('width');
		}
	}
	get unclosable(): boolean {
		return this.getAttribute('unclosable') !== null;
	}
	set unclosable(v: boolean) {
		if (v) {
			this.setAttribute('value', '');
		} else {
			this.removeAttribute('value');
		}
	}

	connectedCallback(){
		this._ro = new ResizeObserver(_ => {
			if (this.opened) { this.position = this.width; }
		});
		this._ro.observe(this._buttons);
		//插入
	}
	disconnectedCallback(){
		if (!this._ro) { return; }
		this._ro.disconnect();
		this._ro = undefined;
		//移除
	}
	adoptedCallback(){
		//移动到其他文档中
	}
	attributeChangedCallback(attrName: string, _oldVal: string | null, newVal: string | null){
		//属性值改变
		switch(attrName) {
			case 'width': 
				this._buttons.style.width = Number(newVal) > 0? newVal + 'px' : null;
				return;
			case 'opened':
				if ((newVal === null) === this.opened) {
					this.opened ? this.close() : this.open();
				}
				return;
		}
	}
}