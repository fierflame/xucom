const pathFn = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const isDevelopment = process.env.NODE_ENV === 'development';
const extractCSS = new ExtractTextPlugin('[name].css');
const loaders = {
    file: { loader: 'file-loader', options: {name: '[path][name].[ext]'}, },
    babel: { loader: 'babel-loader', },
    css: { loader: 'css-loader', },
    less: { loader: 'less-loader', },
    postcss: { loader: 'postcss-loader', },
    style: { loader: 'style-loader', },
}
function cssLoader(...use) {
    return extractCSS.extract({ use: [loaders.css, loaders.postcss, ...use] });
}
module.exports = {
	mode: isDevelopment ? 'development' : 'production',
	watch: isDevelopment,
	devtool: 'source-map',
    entry: {
		xucom: './src/index',
	},
	output: {
		path: pathFn.resolve(__dirname, 'dist'),
        filename: '[name].js',
        library: 'XuCom',
        libraryTarget: 'umd',
	},
    module: {
        strictExportPresence: true,
        rules: [
            { test: /\.(jpg|jpeg|png|gif|tiff|ico|svg|eot|otf|ttf|woff|woff2)$/i, use: [ loaders.file ] },
            { test: /\.([jt]sx?|mjs)$/i, use: [ loaders.babel ], exclude: /node_modules/, },
            { test: /\.less$/, use: cssLoader(loaders.less), },
            { test: /\.css$/, use: cssLoader(), },
        ]
    },
    resolve: {
        extensions: [
            '.tsx', '.ts',
            '.jsx', '.mjs', '.js',
            '.less', '.scss', '.sass', '.css',
        ],
        modules: [ 'node_modules' ],
    },
    plugins: [
		extractCSS,
		// isProduction && new uglify(),
    ].filter(Boolean),
    externals: {
    }
}