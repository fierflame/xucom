export default class Component extends (HTMLElement as {new(): HTMLElement & {shadowRoot: ShadowRoot;}}) {
	static readonly _template?: HTMLTemplateElement;
	static readonly _defaultTagName: string = '';
	static _tagName: string = '';
	static get tagName() { return this._tagName; }
	static set tagName(name) { this.registerCustomElement(name); }
	static registerCustomElement(tagName?: string) {
		if (this._tagName) { return this._tagName; }
		if (!(tagName && typeof tagName === 'string')) { tagName = 'xucom' + this._defaultTagName; }
		tagName = tagName
			.replace(/[^a-zA-Z0-9\-]/g, '')
			.replace(/([A-Z])/g, '-$1')
			.toLowerCase()
			.replace(/(^|-)-+/g, '$1')
			.replace(/-$/, '');
		if (-1 === tagName.indexOf('-')) { tagName = 'xucom-' + tagName; }
		try {
			customElements.define(tagName, this);
			this._tagName = tagName;
			return this._tagName;
		} catch(e) {
			return '';
		}
	}
	constructor() {
		super();
		const template = new.target._template;
		if (template) {
			let shadow = this.attachShadow({mode:'open'});
			shadow.appendChild(document.importNode(template.content, true));
		}
	}
}
